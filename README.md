# webex-connectme

WebEx Teams doesn't have public spaces. you must be invited to any and all spaces you are a member of. This bot aims to change that, by functioning as a searchable repository of "public" spaces, allowing you to be invited to teams by subscribing.